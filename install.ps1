Function GenerateCert() {
  $cert = New-SelfSignedCertificate -DnsName test.contoso.com -CertStoreLocation cert:\LocalMachine\My -KeyExportPolicy Exportable

  $certData = New-Object -TypeName psobject
  $certData | Add-Member -NotePropertyName thumbprint -NotePropertyValue $cert.Thumbprint
  $certData | Add-Member -NotePropertyName notBefore -NotePropertyValue $cert.NotBefore
  $certData | Add-Member -NotePropertyName notAfter -NotePropertyValue $cert.NotAfter

  return $certData
}


# Check if Chocolatey is already installed
$chocoInstalled = $true
if ((Get-Command "choco.exe" -ErrorAction SilentlyContinue) -eq $null) 
{

  $chocoInstalled = $false
  .\install_choco.ps1
}

# update version in app config
$version = $args[0]
$updateURL = $args[1]
$clientPort = $args[2]

$configFolder = "$env:LOCALAPPDATA\CERNAppStore"
$configPath = "$env:LOCALAPPDATA\CERNAppStore\appConfig.json"
$logFolder = "$env:LOCALAPPDATA\CERNAppStore\logs"

# Create config folder it it doesn't exist
If(!(Test-Path $configFolder))
{
  New-Item -ItemType Directory -Force -Path $configFolder
  New-Item -ItemType Directory -Force -Path $logFolder
}

if(Test-Path $configPath -PathType Leaf) {
  # Update the config if it exists
  $jsonString = Get-Content -Path $configPath -Raw
  $json = ConvertFrom-Json -InputObject $jsonString

  $json.version = $version
  $json.updateURL = $updateURL

  $jsonString = ConvertTo-Json -InputObject $json -Depth 100
} else {
  # Otherwise, create a default config with a certificate
  $cert = GenerateCert

  $defaultConfig = @{
    client = @{
      port = $clientPort
      platform = "windows"
    }
    monitor = @{
      apps = @()
    }
    version = $version
    updateURL = $updateURL
    chocoInstalled = $chocoInstalled
    repos = @()
    cert = $cert
    installed = @{
      packages = @()
    }
  }

  $jsonString = ConvertTo-Json -InputObject $defaultConfig -Depth 100
}

# Use this method to write the file
# This prevents powershell from inserting BOM (Byte-Order-Mark)
# at the beginning of the file which makes the JSON invalid
[IO.File]::WriteAllLines($configPath, $jsonString)

# make registry changes
reg import ".\register.reg"

# schedule task

$taskName = "CERNAppStoreService"
$taskSchedule = New-ScheduledTaskTrigger -AtLogOn
if ($PSVersionTable.PSVersion.Major -eq 4) {
  $taskSchedule.RandomDelay = New-TimeSpan -Seconds 30
} else {
  $taskSchedule.Delay = "PT30S"
}
$taskSettings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries
$taskAction = New-ScheduledTaskAction -Execute "`"C:\Program Files\CERNAppStore\service\ServiceRunner\ServiceRunner.exe`""
Register-ScheduledTask -TaskName $taskName -Action $taskAction -Trigger $taskSchedule -Settings $taskSettings -RunLevel Highest -Force

# run the .NET app
Start-ScheduledTask -TaskName $taskName