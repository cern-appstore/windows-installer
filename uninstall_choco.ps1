# Remove also Chocolatey if it was installed by our installer
$configPath = "$env:LOCALAPPDATA\CERNAppStore\appConfig.json"

Function RemoveSources($json) {
  if ((Get-Command "choco.exe" -ErrorAction SilentlyContinue) -eq $null) {
    foreach ($repo in $json.repos) {
      choco source remove -n=$repo -y
    }
  }
} 

Function RemoveChoco($json) {
  if(!$json.chocoInstalled) {
      Remove-Item "C:\ProgramData\chocolatey" -Recurse
  }
} 

Function RemoveCert($json) {
  $port = $json.client.port
  netsh http delete sslcert ipport="0.0.0.0:$port"
} 

if(Test-Path $configPath -PathType Leaf) {
  $jsonString = Get-Content -Path $configPath -Raw
  $json = ConvertFrom-Json -InputObject $jsonString

  try {
    RemoveSources($json)
  } catch {
    #Failed to remove sources
  }

  try {
    RemoveChoco($json)
  } catch {
    #Failed to remove choco
  }

  try {
    RemoveCert($json)
  } catch {
    #Failed to remove cert
  }
}