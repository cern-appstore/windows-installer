param(
  [switch]
  $Signed
)

# prevents slow downloads (https://stackoverflow.com/questions/28682642/powershell-why-is-using-invoke-webrequest-much-slower-than-a-browser-download)
$ProgressPreference = "SilentlyContinue"
# download the artifacts
Invoke-WebRequest https://gitlab.cern.ch/api/v4/projects/62798/jobs/artifacts/master/download?job=build_dotnet -OutFile .\build.zip -Headers @{"PRIVATE-TOKEN"="$env:PRIVATE_KEY"}
# unzip and place into correct folders
Expand-Archive -LiteralPath .\build.zip -DestinationPath .
Rename-Item .\build .\service
Remove-Item .\build.zip
Invoke-WebRequest https://gitlab.cern.ch/api/v4/projects/54070/jobs/artifacts/master/download?job=build_windows -OutFile .\build.zip -Headers @{"PRIVATE-TOKEN"="$env:PRIVATE_KEY"}
Expand-Archive -LiteralPath .\build.zip -DestinationPath .
Rename-Item .\build_windows .\client
Remove-Item .\build.zip

# ================================ #
# ================================ #
# run inno to create the installer #

# master commits have no tags associated, so just
# set some large enough version number so that it doesn't auto-update
$version = $env:CI_COMMIT_TAG
if($env:CI_COMMIT_TAG -eq $null)
{
  $version = "1000.0.0"
}


# create the installer
& "C:\Program Files (x86)\Inno Setup 6\ISCC.exe" /DVERSION_NUMBER=$version /DUPDATE_URL=$env:UPDATE_URL /DCLIENT_PORT=$env:CLIENT_PORT .\installer.iss
# sign the installer
# temporary workaround to sign exe with EV certificate
$localExePath = Get-ChildItem CERNAppStoreSetup.exe | ForEach-Object { $_.FullName }
$fqdnExePath = "\\$($env:computername)\$localExePAth".Replace(":","$")
Write-Host $fqdnExePath
Send-MailMessage -To "sifirooz@cern.ch" -From "no-reply@cern.ch" -Subject "[AppStore] Sign Client Build" -Body "$fqdnExePath" -SmtpServer "cernmx.cern.ch" -Encoding UTF8

if ($Signed) {
  $sigStatus = (Get-AuthenticodeSignature .\CERNAppStoreSetup.exe).Status
  $sigTries = 5
  
  while ($sigStatus -ne "Valid" -and $sigTries -gt 0) {
      Start-Sleep 300
  
      $sigStatus = (Get-AuthenticodeSignature .\CERNAppStoreSetup.exe).Status
      $sigTries--
      Write-Host "$sigTries - $(Get-Date)"
  }
  
  # check if the exe is signed
  # if not, stop the pipeline with an error
  if($sigStatus -ne "Valid") {
      throw "Failed to sign the installer, status: $sigStatus"
  }
}
