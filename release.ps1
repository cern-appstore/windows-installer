# Creates a new release for the latest tag
$headers = @{"Content-Type"="application/json"; "PRIVATE-TOKEN"="$env:PRIVATE_KEY"}
# body as described here: https://gitlab.cern.ch/help/api/releases/index.md
# $env:CI_COMMIT_TAG is the name of the tag
$body = '{"name":"New release", "tag_name":"' + $env:CI_COMMIT_TAG + '", "description": "Latest release", "assets": { "links": [{"name": "CERNAppStoreSetup.exe", "url": "https://gitlab.cern.ch/cern-appstore/windows-installer/-/jobs/' + $env:CI_JOB_ID +'/artifacts/raw/CERNAppStoreSetup.exe"}] }}'

Invoke-WebRequest "https://gitlab.cern.ch/api/v4/projects/86960/releases" -Headers $headers -Method POST -Body $body
