# Stop the service
Stop-Process -Force -ProcessName CERNAppStoreService
Wait-Process -ProcessName CERNAppStoreService
# Stop the app store
Stop-Process -Force -ProcessName CERNAppStore
Wait-Process -ProcessName CERNAppStore

# remove registry changes
reg import ".\unregister.reg"

# Remove the scheduled task
# schtasks /end /tn "AppStoreNETClient"
schtasks /delete /tn "CERNAppStoreService" /F

# Wait-Process does not guarantee that
# the file handles will be released
Function WaitLockReleased($path) {
  $nAttempts = 5
  for($i = 0; $i -lt $nAttempts; $i++)
  {
    if(Test-Path $path -PathType Leaf) {
      try {
        [IO.File]::OpenWrite($path).close();
        break
      } catch {
        # still locked
      }
    } else {
      break
    }
  
    Start-Sleep -Milliseconds 500
  }
} 

# Wait until we can safely remove the files
WaitLockReleased("C:\Program Files\CERNAppStore\client\CERNAppStore.exe")
WaitLockReleased("C:\Program Files\CERNAppStore\service\CERNAppStoreService.exe")