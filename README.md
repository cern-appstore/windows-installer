# Useful links
[Setting up Windows CI machines](https://gitlab.cern.ch/cern-appstore/windows-installer/-/wikis/How-to-setup-custom-Windows-CI-machines)

# Windows installer 

This project creates a unified installer for the
two desktop apps:

- https://gitlab.cern.ch/mam-mdm/mam-windows-client
- https://gitlab.cern.ch/mam-mdm/cern-mam-electron-poc-frontend-store


This project is powered by [Inno Setup](https://www.jrsoftware.org/isinfo.php).
The source config for the installer with some helpful comments is in the file `installer.iss`.

# How to use
## Create installer manually

- Copy the build files of the .NET service into a folder named `service`.
- Copy the build files of the electron client into a folder named `client`.
- Compile `installer.iss` with Inno
  - You need to pass additional arguments to the comiler:
    - `/DVERSION_NUMBER=$version`
    - `/DUPDATE_URL=$updateURL`
    - `/DCLIENT_PORT=$clientPort`
  - e.g. `ISCC.exe /DVERSION_NUMBER=0.1.1 /DUPDATE_URL=http://example.com /DCLIENT_PORT=8080 installer.iss`
- A file named `CERNAppStoreSetup.exe` will be generated in the root folder of this project

## Create installer via Gitlab CI

This project takes advantage of Gitlab CI and a new installer will be built automatically whenever
there is a new tag or a new commit to `master`.
Pushing to `master` in any of the dependent projects (electron, C#) will also trigger the installer to be rebuilt.
Further, when a new tag is pushed the CI will create a new release with
the version specified by the tag and the corresponding (signed) `.exe` file available for download.

For `master` commits, the installer artifact can be found on this
[link](https://gitlab.cern.ch/cern-appstore/windows-installer/-/jobs/artifacts/master/raw/AppStoreSetup.exe?job=build_master).
These artifacts have the version number set to **1000.0.0**.

For tags, the installer can be downloaded from the releases page directly.

The powershell script [build.ps1](build.ps1) is a part of the CI and is responsible for downloading the repositories and building the installer with `ISCC`.

The script [release.ps1](release.ps1) creates a new release using the Gitlab API.

The CI needs a PAT (Private Access Token) to authenticate. `$CI_JOB_TOKEN` is only available in Gitlab Premium. The token resides in the `PRIVATE_KEY` variable.

We use a simple Windows shell executor for this project since we need Windows to build. Do not enable shared runners as they use Linux.

## Code signing

The installer is automatically signed using [signtool](https://docs.microsoft.com/en-us/windows/win32/seccrypto/signtool). See [build.ps1](build.ps1).
The certificate should be located at this network location:
- `\\cern.ch\dfs\Services\CERNAppStore\certs\cert-valid-20230227.pfx`

## Setting up CI

### Environment variables

- __PRIVATE_KEY__ - personal access token required to download artifacts from other repos
- __CLIENT_PORT__ - the default port on which the C# service listens (8000) (should be changed)
- __UPDATE_URL__ - the url from which the updates are downloaded
- __SIGN_CERT_PW__ - the cert password used for code signing

The machines used for CI need to be Windows machines configured as shell executors. They should have Inno verion 6 installed
(`C:\Program Files (x86)\Inno Setup 6\ISCC.exe`) together with the signtool in
`C:\Program Files (x86)\Windows Kits\10\App Certification Kit\signtool.exe`.

### Troubleshooting

- Verify that the PAT is not expired
- Verify that certificate can be found at the specified location (See [build.ps1](build.ps1))
- Verify that certificate is valid
- Verify that the signtool.exe is in the specified location (See [build.ps1](build.ps1))
- Verify that Inno is installed

## Installer

The generated installer installs the app into `C:\Program Files\CERNAppStore`.
The uninstaller can also be found in this folder.
The installer creates desktop and start menu shortcuts for the electron app.
The .NET service is registered to run at logon with elevated privileges and it is started immediately after installing. A new key in the registry is created to allow opening links on the website with the app.
The installer also installs chocolatey on the system if it is not present.
Finally, it will update the app version in `%LOCALAPPDATA%\CERNAppStore\appConfig.json`
(or create a new file if not present) and generate a new SSL certificate.

When uninstalling, all files including `%LOCALAPPDATA%/CERNAppStore` are deleted.
The .NET service is stopped, the scheduled task is removed and registry keys are deleted.
The assigned certificate is removed and all added Chocolatey sources are removed together with Chocolatey
itself if it was previously installed by this installer.

## Updating

The installer is capable of updating an older version of the application. It will first stop the .NET service and remove all files from the `Program Files` directory.
It then proceeds with normal installation.

## Pipeline diagram

![pipeline](pipeline.png)

## Version propagation from git tags

![tag](version_propagation.png)
